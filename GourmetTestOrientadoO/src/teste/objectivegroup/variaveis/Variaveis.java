
package teste.objectivegroup.variaveis;

public class Variaveis {
 
    private String pratoPensado;
    private String pratoSugerido;
    private String resposta;

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public String getPratoPensado() {
        return pratoPensado;
    }

    public void setPratoPensado(String pratoPensado) {
        this.pratoPensado = pratoPensado;
    }

    public String getPratoSugerido() {
        return pratoSugerido;
    }

    public void setPratoSugerido(String pratoSugerido) {
        this.pratoSugerido = pratoSugerido;
    }


  

    
}
